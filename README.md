
<!-- README.md is generated from README.Rmd. Please edit that file -->

# wranglR

<!-- badges: start -->
<!-- badges: end -->

The goal of wranglR is to simplify and standardize data wrangling tasks
that are performed on a regular basis. The intended use of the package
is:

- automated data pipelines that perform the same data wrangling
- data exploration that will require a known output of data elements

## Installation

You can install the development version of hiaawrangling like so:

`remotes::install_git("https://codeberg.org/ryangarnett/wranglR", git = "external")`

## Functions

The most common data wrangling functions within the package.

`add_day_week` add day of week column to data frame from date column

`add_insert_date` add insert timestamp column to data frame from system
time

`add_week_year` add week of year column to data frame from date column

`create_ymd_parquet_partition` add group columns to data frame from date
column

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(wranglR)

# Using add_day_week function
df |>
  wranglR::add_day_week(LOCAL_DATE)
#>   PLACE_ID LOCAL_DATE DAY_OF_WEEK
#> 1   ABC123 2023-04-30      Sunday
#> 2   ABC123 2023-05-01      Monday
#> 3   ABC123 2023-05-02     Tuesday
#> 4   ABC123 2023-05-03   Wednesday
#> 5   ABC123 2023-05-04    Thursday
#> 6   ABC123 2023-05-05      Friday
#> 7   ABC123 2023-05-06    Saturday


# Using add_insert_date function
df |>
  wranglR::add_insert_date()
#>   PLACE_ID LOCAL_DATE         INSERT_DATE
#> 1   ABC123 2023-04-30 2023-05-31 06:30:35
#> 2   ABC123 2023-05-01 2023-05-31 06:30:35
#> 3   ABC123 2023-05-02 2023-05-31 06:30:35
#> 4   ABC123 2023-05-03 2023-05-31 06:30:35
#> 5   ABC123 2023-05-04 2023-05-31 06:30:35
#> 6   ABC123 2023-05-05 2023-05-31 06:30:35
#> 7   ABC123 2023-05-06 2023-05-31 06:30:35


# Using add_week_year function
df |>
  wranglR::add_week_year(LOCAL_DATE)
#>   PLACE_ID LOCAL_DATE WEEK_OF_YEAR
#> 1   ABC123 2023-04-30           18
#> 2   ABC123 2023-05-01           18
#> 3   ABC123 2023-05-02           18
#> 4   ABC123 2023-05-03           18
#> 5   ABC123 2023-05-04           18
#> 6   ABC123 2023-05-05           18
#> 7   ABC123 2023-05-06           18


# Using create_ymd_parquet_partition function
df |>
  wranglR::create_ymd_parquet_partition(LOCAL_DATE)
#> # A tibble: 7 × 5
#> # Groups:   YEAR, MONTH, DAY [7]
#>   PLACE_ID LOCAL_DATE  YEAR MONTH   DAY
#>   <chr>    <date>     <dbl> <dbl> <int>
#> 1 ABC123   2023-04-30  2023     4    30
#> 2 ABC123   2023-05-01  2023     5     1
#> 3 ABC123   2023-05-02  2023     5     2
#> 4 ABC123   2023-05-03  2023     5     3
#> 5 ABC123   2023-05-04  2023     5     4
#> 6 ABC123   2023-05-05  2023     5     5
#> 7 ABC123   2023-05-06  2023     5     6
```

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date. `devtools::build_readme()` is handy for this. You could also
use GitHub Actions to re-render `README.Rmd` every time you push. An
example workflow can be found here:
<https://github.com/r-lib/actions/tree/v1/examples>.
