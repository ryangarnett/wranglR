#' Add day of week column to data frame from date column
#' 
#' This function creates a new data frame column with
#' day of the week (i.e. Monday, Tuesday, Wednesday, etc.) information
#' that is calculated from a provided date column
#' 
#' @param date_column A column with date or datetime data type
#' 
#' @return Returns a character column with day of the week (i.e. Monday, Tuesday, etc.)
#' 
#' @examples
#' \donttest{
#' df |> add_day_week(LOCAL_DATE)
#' }
#' 
#' @export
add_day_week <- function(.data, date_column){
  .data |>
    dplyr::mutate(DAY_OF_WEEK = lubridate::wday({{date_column}}, label = TRUE, abbr = FALSE))
}
