#' Add group columns to data frame from date column
#' 
#' This function creates group columns to a data frame column
#' from a provided date column
#' 
#' @param date_column A column with date or datetime data type
#' 
#' @return Returns grouped columns for year, month, and day
#' 
#' @examples
#' \donttest{
#' df |> create_ymd_parquet_partition(LOCAL_DATE)
#' }
#' 
#' @export
create_ymd_parquet_partition <- function(.data, date_column){
  .data |>
    dplyr::group_by(YEAR = lubridate::year({{date_column}}),
                    MONTH = lubridate::month({{date_column}}),
                    DAY = lubridate::day({{date_column}})) 
}
